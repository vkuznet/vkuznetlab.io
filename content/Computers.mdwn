### Computers

During my career in High-Energy Phyics (HEP) I worked on numerous projects
related to data discovery, data management, data transfer and aggregation
systems, analytics, (meta)-data-services, and various web application projects.

Here I provide brief overview of projects I was involved with.

##### Request Manager for Data Management and Workflow Management group of CMS experiment
The goal of the project is scheduling of workflows execution
on a GRID infrastructure requested by data operational team.

##### WMArchive for CMS experiment
The system was designed to reliably store 1M documents on a daily basis
to HDFS and provide aggregation dashboards and search capabilities
across 300M documents (yearly statistics).
You may find more details about the system in published paper:

_The archive solution for distributed workflow management agents of the CMS
experiment at LHC_, V. Kuznetsov, N. Fischer, Y. Guo Computing and Software for
Big Science.

##### Data Aggregation system (DAS) for CMS experiment
The system was designed to search and aggregated various information
from distributed set of data-services (based on RDBMS solution).
We implemented custom Query Language (similar to SQL) as well as
caching layer among distributed databases. Full details about DAS
can be found in the following set of papers:

_Keyword Search over Data Service Integration for Accurate Results_
V. Zemleris, V. Kuznetsov and R. Gwadera, J. Phys.: Conf. Ser. 513 032106 doi:10.1088/1742-6596/513/3/032106

_Life in extra dimensions of database world or penetration of NoSQL in HEP community_ 
V Kuznetsov et al., J. Phys.: Conf. Ser. 396 052043 2012, doi:10.1088/1742-6596/396/5/052043

_Data Aggregation System - a system for information retrieval on demand
over relational and non-relational distributed data sources_
G. Ball, V. Kuznetsov, D. Evans and S. Metson, doi:10.1088/1742-6596/331/4/042029

_The CMS Data Aggregation System_
V. Kuznetsov, D. Evans, S. Metson, doi:10.1016/j.procs.2010.04.172

##### CMS Data Bookkeeping System (DBS) and DBS Query Language
The Data Bookkeeping System (DBS) for CMS experiment served as a main
meta-data catalog of all data produced by the experiment, including
datasets, blocks, files, run and lumi-sections. The description of the
system can be found in the following papers:

_The CMS Dataset Bookkeeping Service_
A. Afaq, et. al., J. Phys.: Conf. Ser. Volumne 119, 072001, 2008

_The CMS DBS query language_
A. Afaq, V. Kuznetsov, L. Lueking, D.Riley, V. Sekhri, doi:10.1088/1742-6596/219/4/042043

##### Event Store system for Cleo-c experiment
We designed and built meta-data data-service
for Cleo-c experiment. Its details can be found here:

_The New EventStore Data Management System For The CLEO-c Experiment_
C.D. Jones, V. Kuznetsov, D. Riley, G.J. Sharp, Int. J. Mod. Phys. A20:3868-3870, 2005

##### Miscellaneous projects
- trace-back debugging system
- migration of Cleo-c softwarwe from OSF to Solaris/Linux
- data transfer system
- tracking software based on Kalman filter for D0 experiment
- tracking and vertex reconstruction software for NOMAD-STAR experiment.
